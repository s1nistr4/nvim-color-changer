"""
Generates random color scheme every time you open vim
"""

# Settings
enabled = True # is it enabled? If not, set this to false. This makes it so the alias doesn't break
colors = ['ayu', 'gruvbox', 'github_dark', 'nightfox', 'kimbox', 'cobalt2', 'vscode', 'eva01',  'hatsunemiku']

from random import choice
import os

def main(colors):
    # colors you want to cycle through, yes ik this sucks for now but it will be made automatic someday :)
    colors = choice(colors)
    index = 0

    with open(os.environ.get('VIM_CONFIG_LOCATION'), 'r+') as f:
        for i in f.readlines():
            data = f.read(index)
            index += 1

            if 'colorscheme' in i:
                found_line = i.split()[1]
                found_text = f"colorscheme {found_line}"
                
                f.seek(0)
                file_data = f.read()

                with open(os.environ.get("VIM_CONFIG_LOCATION"), 'w') as f2:
                    new = file_data.replace(found_text, f"colorscheme {colors}")
                    f2.write(new)

                print("updated successfully")

if __name__ == "__main__":
    if enabled == True:
        main(colors)
    else:
        exit()

else:
    print("please run this file directly.")
    exit()
