# nvim-color-changer
hacky color changer for neovim

# how
just add this to your .bashrc

```
VIM_CONFIG_LOCATION=<location of your ~/.config/nvim/ config, absolute path>
alias nvim="python3 <location_of_color_changer>.py; nvim"
```

--- 
(c) s1nistr4
